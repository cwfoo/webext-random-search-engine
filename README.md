# Random Search Engine
Firefox extension that randomly chooses a search engine for each web search.


## Usage
After installation, select "Random Search Engine" as your default search engine.


## Development
Run `make` to build `_build/random-search-engine.xpi`.


## License
This project is distributed under the terms of the BSD 2-Clause License (see
[LICENSE](./LICENSE)).


## Contributing
Bug reports, suggestions, and patches should be submitted on Codeberg:
https://codeberg.org/cwfoo/webext-random-search-engine
