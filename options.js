function saveOptions(event) {
    event.preventDefault();

    const urlTemplates = document.getElementById("search-engines").value
        .split('\n')
        .map(line => line.trim())
        .filter(line => line.length > 0);

    // Input validation.
    if (urlTemplates.length == 0) {
        alert("Error: Please add at least one search engine");
        return;
    }
    urlTemplates.forEach((template, index) => {
        if (!template.includes("%s")) {
            alert(`Error: Line ${index + 1}\n${template}\nMissing %s placeholder`);
            return;
        }
    });

    browser.storage.local.set({urlTemplates: urlTemplates});
}

function restoreOptions() {
    browser.storage.local.get(result => {
        document.getElementById("search-engines").value = result.urlTemplates.join('\n');
    });
}

document.querySelector("form").addEventListener("submit", saveOptions);
document.addEventListener("DOMContentLoaded", restoreOptions);
