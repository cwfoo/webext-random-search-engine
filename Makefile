.POSIX:

source_files=	manifest.json \
		options.html \
		options.js \
		random-search-engine.js \
		LICENSE \
		README.md

all: _build/random-search-engine.xpi

_build/random-search-engine.xpi: $(source_files)
	@mkdir -p _build/
	zip -r -FS $@ $(source_files)

clean:
	rm -rf ./_build/
