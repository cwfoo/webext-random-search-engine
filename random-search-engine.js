const prefix = "https://random.search.invalid/?q=";

let urlTemplates = [
    "https://duckduckgo.com/?q=%s",
    "https://www.startpage.com/do/dsearch?query=%s",
    "https://www.qwant.com/?q=%s"
];

// Add default random search engines on installation.
browser.runtime.onInstalled.addListener((details) => {
    if (details.reason == "install") {
        browser.storage.local.set({urlTemplates: urlTemplates});
    }
});

// Get user preferences.
browser.storage.local.get(data => {
    urlTemplates = data.urlTemplates;
});
browser.storage.onChanged.addListener(changeData => {
    urlTemplates = changeData.urlTemplates.newValue;
});

// Returns a random element from the given array.
function randomChoice(array) {
    return array[Math.floor(Math.random() * array.length)];
}

function redirectHandler(request) {
    const searchString = request.url.slice(prefix.length);
    const chosenUrlTemplate = randomChoice(urlTemplates);
    return {
        redirectUrl: chosenUrlTemplate.replace("%s", searchString)
    };
}

browser.webRequest.onBeforeRequest.addListener(
    redirectHandler,
    {urls: [prefix + "*"]},
    ["blocking"]
);
